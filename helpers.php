<?php

use \Moapi\Api;

function api_error($msg, $code = 400, $data = [])
{
    (new Api())->api_error($msg, $code, $data);
}

function error_build(string $message = '', int $code = 400, $data = null)
{
    return (new Api())->error_build($message, $code, $data);
}

function error_check($data, $checkOnly = false)
{
    return (new Api())->error_check($data, $checkOnly);
}

if (!function_exists('mstime')) {
    /**
     * 获取毫秒时间戳
     * @return int
     */
    function mstime(): int
    {
        [$s1, $s2] = explode(' ', microtime());
        return (int)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }

}
