<?php

namespace Moapi;


/**
 * 配置方法
 * @package App\Http\Controllers\Admin
 */
class Config
{
    static function getBasePath()
    {
        return dirname(dirname(dirname(dirname(__DIR__))));
    }

    /**
     * 获取项目列表
     * @return \Laravel\Lumen\Application|mixed|string[]
     */
    static function getProjectList()
    {
        return config('moapi.routes') ?: ['/' => '\App\Http\Controllers'];
    }

    /**
     * 获取项目详情
     * @param $key
     * @return mixed|object|string
     */
    static function getProjectNamespace($key)
    {
        $config = self::getProjectList();
        return $config['/' . $key] ?? $config['/'];
    }

    static function getIgnoreClass($project)
    {
        //  系统预置的
        $default = [
            'Laravel\\Lumen\\Routing\\Controller',
        ];
        //  客户端配置
        $custom = config("moapi.docs.$project.ignore.class") ?: [];
        return array_merge($default, $custom);
    }

    static function getIgnoreMethod($project)
    {
        //  系统预置的
        $default = [
            '__construct',
        ];
        //  客户端配置
        $custom = config("moapi.docs.$project.ignore.method") ?: [];
        return array_merge($default, $custom);
    }
}
