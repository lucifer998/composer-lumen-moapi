<?php

namespace Moapi;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{

    /**
     * 注册绑定到容器中
     *
     * @return void
     */
    public function register()
    {
        //  新增api的返回内容
        $this->app->middleware(\Moapi\Middleware\ResponseMiddleware::class);

        $this->app->configure('moapi');

        //  校验方法是否存在
//        if (!(new Routes())->routesCheck((new Routes())->getRoutes()))
//            api_error('方法不存在');
        //  处理自定义路由信息
        $this->app->router->addRoute(['GET', 'POST'], (new Routes())->getProjectUri(), (new Routes())->getRoutes());

        //  处理自动化文档的api接口
        $this->app->router->addRoute(['GET', 'POST'], '/docs/list', '\\Moapi\\Controllers\\DocsController@list');
        $this->app->router->addRoute(['GET', 'POST'], '/docs/detail', '\\Moapi\\Controllers\\DocsController@detail');
    }
}
