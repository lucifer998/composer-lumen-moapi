<?php

namespace Moapi\Service;


use Moapi\Config;
use Moapi\Routes;

/**
 * 自动化文档服务
 * @package Chenmoye\Laravel\Service
 */
class DocsService
{
    protected $ignoreClass;
    protected $ignoreMethod;
    protected $projectNamespace;
    protected $projectPath;

    public function __construct()
    {
        $this->projectNamespace = (new Routes())->getProjectNamespace();
        $this->projectPath = Config::getBasePath() . '/' . implode('/', explode('\\', $this->projectNamespace));
        $this->ignoreClass = Config::getIgnoreClass((new Routes())->getProjectUri());
        $this->ignoreMethod = Config::getIgnoreMethod((new Routes())->getProjectUri());
    }

    /**
     * 获取菜单
     * @return mixed
     */
    public function getMenu()
    {
        return $this->getMenuData($this->getFileList($this->projectPath));
    }

    public function getDetail($route)
    {
        $service = (new Routes())->getService();
        $data = [
            'service' => $service,
            'api' => (new Routes())->getHostUrl() . '?s=' . $service,
        ];
        $className = explode('@', $route)[0];
        $methodName = explode('@', $route)[1];

        //  获取类的信息
        $class = new \ReflectionClass($className);

        //  获取方法信息
        $method = $class->getMethod($methodName);
        $classProperties = $class->getDefaultProperties();
        $requestRule = $classProperties['requestRule'];
        $requestRule = $requestRule[$methodName] ?? [];
        $data['request'] = $this->formatRequestRule($requestRule);
        //  获取方法备注
        $data['comment'] = $this->getMethodComment($method->getDocComment());
        $data['response'] = [];

        return $data;
    }

    public function formatRequestRule($requestRule)
    {
        $requestRule = (new FilterService())->ruleInit($requestRule);
        $list = [];
        foreach ($requestRule as $k => &$v) {
            if (isset($v['children']))
                $v['children'] = $this->formatRequestRule($v['children']);
            $list[] = $v;
        }
        return $list;
    }

    /**
     * 获取菜单内容
     * @param $tree
     * @param string $space
     * @return mixed
     */
    private function getMenuData($tree, $space = '')
    {
        $list = [];
        foreach ($tree as $k => $v) {
            if (is_array($v)) {
                $list[] = [
                    'name' => $k,
                    'type' => 'menu',
                    'child' => $this->getMenuData($v, $space ? $space . '\\' . $k : $k)
                ];
            } else {
                $list[] = $this->getClassData($space ? $space . '\\' . $v : $v);
            }
        }
        return $list;
    }

    /**
     * 获取所有文件列表
     * @param $path
     * @return array|array[]
     */
    public function getFileList($path)
    {
        $list = [];
        $pathFile = glob($path . '/*');
        foreach ($pathFile as $v) {
            if (is_dir($v)) {
                $list[str_replace($path . '/', '', $v)] = $this->getFileList($v);
            } else {
                $className = str_replace('Controller.php', '', str_replace($path . '/', '', $v));
                if ($className)
                    $list[] = $className;
            }
        }
        return $list;
    }

    public function getClassData($namespace)
    {
        $class = new \ReflectionClass($this->projectNamespace . '\\' . $namespace . 'Controller');
        //  获取过滤的控制器列表
        $methodList = [];
        foreach ($class->getMethods(\ReflectionProperty::IS_PUBLIC) as $v) {
            if (!in_array($v->class, $this->ignoreClass) && !in_array($v->name, $this->ignoreMethod)) {
                $methodData = [
                    'type' => 'method',
                    'name' => $v->name,
                    'comment' => $this->getMethodComment($class->getMethod($v->name)->getDocComment()),
                    'service' => implode('.', explode('\\', $namespace)) . '.' . $v->name
                ];
                $methodList[] = $methodData;
            }
        }

        $classData = [
            'type' => 'class',
            'name' => str_replace('Controller', '', $class->getShortName()),
            'comment' => $this->getClassComment($class->getDocComment()),
            'child' => $methodList
        ];
        return $classData;
    }

    public function getMethodData($namespace, $method)
    {

    }

    /**
     * 获取class的备注
     * @param $comment
     * @return string[]
     */
    public function getClassComment($comment)
    {
        $data = [
            'title' => '无备注'
        ];
        if ($comment) {
            $comment = explode("\n", $comment);
            $data['title'] = trim(str_replace('*', '', trim($comment[1])));
        }
        return $data;
    }

    /**
     * @param $comment
     * @return string[]
     */
    public function getMethodComment($comment)
    {
        $data = [
            'title' => '无备注',
            'desc' => '无描述'
        ];
        if ($comment) {
            //  获取返回参数
            $comment = explode("\n", str_replace('*', '', $comment));
            $data['title'] = trim($comment[1]);
            foreach ($comment as $k => $v) {
                $v = trim($v);
                //  进行描述处理
                if (strpos($v, '@desc') !== false) {
                    $data['desc'] = trim(str_replace('@desc', '', $v));
                };
            }
        }
        return $data;
    }
}
