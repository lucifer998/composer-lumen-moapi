<?php

namespace Moapi\Service;

use App\Exceptions\ApiErrorException;
use Moapi\Routes;
use Illuminate\Http\Request;

/**
 * 参数过滤方法
 * @package App\Http\Controllers\Admin
 */
class FilterService
{
    public function requestFilter($rules)
    {
        $method = (new Routes())->getMethod();
        $rules = array_change_key_case($rules, CASE_LOWER);
        //  设置过滤内容数组
        $rule = $rules[$method] ?? [];
        $rule = $this->ruleInit($rule);
        dd($rule);
        return $this->getDataByRule($rule);
    }

    public function ruleInit($rule)
    {
        foreach ($rule as $k => &$v) {
            $v['type'] = isset($v['type']) ? strtolower($v['type']) : 'string';
            $v['name'] = $v['name'] ?? $k;
            $v['desc'] = $v['desc'] ?? '';
            $v['format'] = $v['format'] ?? '';
            $v['require'] = $v['require'] ?? true;
            $v['message'] = $v['message'] ?? '';
            switch ($v['type']) {
                //  如果是数组，则把子内容进行处理
                case 'array':
                    $v['children'] = $this->ruleInit($v['children'] ?? []);
                    break;
                //  如果是explode
                case 'explode':
                    $v['separator'] = $v['separator'] ?? ',';
                    break;
            }
        }

        return $rule;
    }

    /**
     * @param $rule
     * @param string $father
     * @return \stdClass
     * @throws ApiErrorException
     */
    public function getDataByRule($rule, $father = ''): \stdClass
    {
        $data = new \stdClass();
        foreach ($rule as $k => $v) {
            $inputKey = $father ? ($father . '.' . $k) : $k;
            $data->{$k} = request()->input($inputKey);
            //  校验数据是否有内容
            if ($v['require'] && $data->{$k} === null)
                api_error($v['message'] ?: '缺少必要参数' . $v['name']);
            if ($v['type'] == 'array')
                $data->{$k} = $this->getDataByRule($v['children'], $inputKey);
        }
        return $data;
    }
}
