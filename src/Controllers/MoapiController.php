<?php

namespace Moapi\Controllers;

use App\Exceptions\ApiErrorException;
use Moapi\Routes;
use Moapi\Service\FilterService;
use Illuminate\Http\Request;

/**
 * 参数过滤核心控制器
 * @package App\Http\Controllers\Admin
 */
class MoapiController extends \Laravel\Lumen\Routing\Controller
{
    /**
     * 入参规则
     * @var
     */
    protected $requestRule = [];

    /**
     * 入参处理后内容
     * @var
     */
    protected $requestData;

    protected $method;


    /**
     * MoapiController constructor.
     */
    public function __construct()
    {
        $this->requestData = (new FilterService())->requestFilter($this->requestRule);
        dd($this->requestData);
        $this->method = (new Routes())->getMethod();
        //  处理入参内容
        $this->requestVerify();
    }

    /**
     * 处理入参信息
     * @throws ApiErrorException
     */
    private function requestVerify()
    {
        $serviceList = explode('.', request()->input('s'));
        //  开始获取本控制器的内容
        $action = strtolower(array_pop($serviceList));
        //  设置过滤内容数组
        $allRules = array_change_key_case($this->requestRule, CASE_LOWER);
        $nowRules = $allRules[$action] ?? [];
        //  创建内容参数
        $this->requestData = new \stdClass();
        $this->requestData = $this->getDataByRule($this->ruleInit($nowRules));
    }

    private function ruleInit($rule)
    {
        foreach ($rule as $k => $v) {
            $rule[$k]['type'] = isset($v['type']) ? strtolower($v['type']) : 'string';
            $rule[$k]['name'] = $v['name'] ?? $k;
            $rule[$k]['desc'] = $v['desc'] ?? '';
            $rule[$k]['format'] = $v['format'] ?? '';
            $rule[$k]['require'] = $v['require'] ?? true;
            $rule[$k]['message'] = $v['message'] ?? '';
            switch ($rule[$k]['type']) {
                //  如果是数组，则把子内容进行处理
                case 'array':
                    $rule[$k]['array'] = $this->ruleInit($v['array'] ?? []);
                    break;
                //  如果是explode
                case 'explode':
                    $rule[$k]['separator'] = $v['separator'] ?? ',';
                    break;
            }
        }

        return $rule;
    }

    /**
     * @param $rule
     * @param string $father
     * @return \stdClass
     * @throws ApiErrorException
     */
    private function getDataByRule($rule, $father = ''): \stdClass
    {
        $data = new \stdClass();
        foreach ($rule as $k => $v) {
            $inputKey = $father ? ($father . '.' . $k) : $k;
            $data->{$k} = request()->input($inputKey);
            //  校验数据是否有内容
            if ($v['require'] && $data->{$k} === null)
                api_error($v['message'] ?: '缺少必要参数' . $v['name']);
            if ($v['type'] == 'array')
                $data->{$k} = $this->getDataByRule($v['array'], $inputKey);
        }
        return $data;
    }
}
