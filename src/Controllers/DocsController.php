<?php

namespace Moapi\Controllers;

use Moapi\Config;
use Moapi\Routes;
use Moapi\Service\DocsService;

class DocsController extends \Laravel\Lumen\Routing\Controller
{
    public function list()
    {
        return (new DocsService())->getMenu();
    }

    public function detail()
    {
        return (new DocsService())->getDetail((new Routes())->getRoutes());
    }
}
