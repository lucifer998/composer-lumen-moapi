<?php

namespace Moapi;


use Moapi\Exceptions\ApiErrorException;

/**
 * 参数过滤方法
 * @package App\Http\Controllers\Admin
 */
class Api
{
    public function api_error($msg, $code = 400, $data = [])
    {
        throw new ApiErrorException([
            'code' => $code,
            'msg' => '非法请求：' . $msg,
            'data' => $data
        ]);
    }


    /**
     * 构建业务错误结构体
     * @param $message //错误内容
     * @param int $code //错误码
     * @param null $data //回参
     * @return array
     */
    function error_build(string $message = '', int $code = 400, $data = null): array
    {
        return [
            'errorMsg' => $message,
            'errorCode' => $code,
            'data' => $data
        ];
    }


    /**
     * 检验业务回参是否错误
     * @param $data
     * @param false $checkOnly //是否只校验不自动抛出
     * @return bool|mixed
     */
    function error_check($data, $checkOnly = false)
    {
        if (is_array($data) && $data['errorCode']) {
            if ($checkOnly) {
                return true;
            } else
                $this->api_error($data['errorMsg'], $data['errorCode']);
        } else
            return $data;
    }

}
