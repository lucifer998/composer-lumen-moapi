<?php

namespace Moapi;


use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * 参数过滤方法
 * @package App\Http\Controllers\Admin
 */
class Token
{
    protected $salt;
    protected $iss;
    protected $expireTime;

    public function __construct()
    {
        $this->salt = config('moapi.token.salt') ?: 'moapi';
        $this->iss = config('moapi.token.iss') ?: 'moapi';
        $this->expireTime = config('moapi.token.expire_time') ?: 86400;
    }

    /**
     * 创建Token
     * @param $uid 用户uid
     * @param $expireTime 过期时间
     * @return String
     **/
    public function createToken($uid = null, $claim = [], $expireTime = null)
    {
        $expireTime = $expireTime ?: $this->expireTime;
        $signer = new Sha256();
        $token = (new Builder())->setIssuer($this->iss)
            ->set('uid', $uid)
            ->set('expireTime', time() + $expireTime);

        foreach ($claim as $k => $v)
            $token = $token->set($k, $v);

        $token = $token->sign($signer, $this->salt)
            ->getToken();

        return (string)$token;
    }

    /**
     * 检测Token是否过期与篡改
     * @param token
     * @return boolean
     **/
    public function validateToken($claim = [], $token = null)
    {
        $token = $token ?: request()->header('AUTHORIZATION');
        try {
            $token = @(new Parser())->parse((string)$token);
        } catch (\Exception $ex) {
            return false;
        }
        $signer = new Sha256();
        if (!$token->verify($signer, $this->salt))
            return false; //签名不正确
        $expireTime = $token->getClaim('expireTime');

        $returnData = [
            'uid' => $token->getClaim('uid'),
            'expireTime' => $expireTime
        ];
        //  校验是否过期
        if ($expireTime < time())
            return false;
        //  获取用户指定内容
        foreach ($claim as $v) {
            try {
                $returnData[$v] = $token->getClaim($v);
            } catch (\Exception $ex) {
            }
        }
        return $returnData;
    }
}
